//packages
import 'package:flutter/material.dart';

class BottomTagPanel extends StatelessWidget {
  final List buttonTexts = [
    'תיירות',
    'אריה',
    'חיות',
    'חיות',
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.symmetric(horizontal: 16),
      scrollDirection: Axis.horizontal,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 2.25),
          child: Center(
            child: Container(
              alignment: Alignment.center,
              width: 106,
              height: 32,
              child: Text(
                buttonTexts[index],
                style: TextStyle(
                  fontSize: 21.0,
                  fontFamily: 'Rubik-Regular',
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: index != 1
                    ? Theme.of(context).buttonColor
                    : Theme.of(context).accentColor,
              ),
            ),
          ),
        );
      },
      itemCount: 4,
    );
  }
}
