//packages
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: InkWell(
              child: Icon(
                Icons.list,
                color: Colors.white,
                size: 32,
              ),
              onTap: () {},
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Text(
              'יום חמישי, 12 בפברואר 2019',
              style: TextStyle(
                  fontSize: 17,
                  color: Colors.white,
                  fontFamily: 'LucidaGrande'),
            ),
          ),
        ],
      ),
    );
  }
}
