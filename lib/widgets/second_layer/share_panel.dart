//packages
import 'package:flutter/material.dart';

class SharePanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).dialogBackgroundColor,
                borderRadius: BorderRadius.circular(15),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Color.fromRGBO(31, 31, 31, 0.2),
                    offset: Offset(0, 24.0),
                    blurRadius: 24.0,
                  ),
                ],
              ),
              child: Container(
                height: 41,
                width: 157,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(20, 10, 20, 8),
                      child: Image.asset(
                        'assets/images/share.png',
                        height: 28,
                        color: Colors.white,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 8),
                      child: Image.asset(
                        'assets/images/facebook.png',
                        height: 23,
                        color: Colors.white,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(20, 10, 19, 8),
                      child: Image.asset(
                        'assets/images/twitter.png',
                        height: 23,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
