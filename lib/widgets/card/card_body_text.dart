//packages
import 'package:flutter/material.dart';

class CardBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        padding: EdgeInsets.all(0.0),
        children: <Widget>[
          Text(
            'מונח ה"צבר" הישראלי נטבע על ידי אורי קיסרי ב-18 באפריל 1931 במאמר "אנחנו עלי-הצבר!" ובו טען נגד קיפוח ילידי הארץ כנגד המהגרים החדשים.',
            style: TextStyle(
              fontFamily: 'Assistant-Regular',
              fontSize: 17,
            ),
          ),
          Text(''),
          Text(
            'הפופולריות של מונח זה בתרבות הישראלית התגלגלה גם לבקבוק ליקר הסברה הישראלי, אשר עוצב בדמות האמפורה היוונית, ברוח תנועת "הכנענים".מונח ה"צבר" הישראלי נטבע על ידי אורי קיסרי ב-18 באפריל 1931 במאמר "אנחנו עלי-הצבר!" ובו טען נגד קיפוח ילידי הארץ כנגד המהגרים החדשים.',
            style: TextStyle(
              fontFamily: 'Assistant-Regular',
              fontSize: 17,
            ),
          ),
          Text(''),
          Text(
            'הפופולריות של מונח זה בתרבות הישראלית התגלגלה גם לבקבוק ליקר הסברה הישראלי, אשר עוצב בדמות האמפורה היוונית, ברוח תנועת "הכנענים".מונח ה"צבר" הישראלי נטבע על ידי אורי קיסרי ב-18 באפריל 1931 במאמר "אנחנו עלי-הצבר!" ובו טען נגד קיפוח ילידי הארץ כנגד המהגרים החדשים.',
            style: TextStyle(
              fontFamily: 'Assistant-Regular',
              fontSize: 17,
            ),
          ),
          Text(''),
          Text(
            'הפופולריות של מונח זה בתרבות הישראלית התגלגלה גם לבקבוק ליקר הסברה הישראלי, אשר עוצב בדמות האמפורה היוונית, ברוח תנועת "הכנענים".מונח ה"צבר" הישראלי נטבע על ידי אורי קיסרי ב-18 באפריל 1931 במאמר "אנחנו עלי-הצבר!" ובו טען נגד קיפוח ילידי הארץ כנגד המהגרים החדשים.',
            style: TextStyle(
              fontFamily: 'Assistant-Regular',
              fontSize: 17,
            ),
          ),
          Text(''),
          Text(
            'הפופולריות של מונח זה בתרבות הישראלית התגלגלה גם לבקבוק ליקר הסברה הישראלי, אשר עוצב בדמות האמפורה היוונית, ברוח תנועת "הכנענים".',
            style: TextStyle(
              fontFamily: 'Assistant-Regular',
              fontSize: 17,
            ),
          )
        ],
      ),
    );
  }
}
