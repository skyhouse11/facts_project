//packages
import 'package:flutter/material.dart';

class CardTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      'הידעת ש…',
      style: TextStyle(
        fontSize: 35,
        fontFamily: 'Assistant-Bold',
        fontWeight: FontWeight.bold,
        color: Theme.of(context).textSelectionHandleColor,
      ),
    );
  }
}
