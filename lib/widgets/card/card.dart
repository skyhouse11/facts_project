//packages
import 'package:flutter/material.dart';

//widgets
import '../../widgets/card/card_title_text.dart';
import '../../widgets/card/card_body_text.dart';
import '../../widgets/card/card_buttons_bar.dart';

class CardClass extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(31, 31, 31, 0.2),
            offset: Offset(0, 24.0),
            blurRadius: 24.0,
          ),
        ],
      ),
      child: Card(
        margin: EdgeInsets.all(0.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 7,
              child: Container(
                padding: EdgeInsets.fromLTRB(30, 13, 30, 5),
                child: Row(
                  children: <Widget>[
                    CardTitle(),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 40,
              child: Container(
                padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                child: CardBody(),
              ),
            ),
            Expanded(
              flex: 12,
              child: Container(
                child: CardButtonsBar(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
