//packages
import 'package:flutter/material.dart';

//widgets
import '../../widgets/background/bottom_tag_panel.dart';

class BackgroundLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          alignment: Alignment.topCenter,
          color: Color.fromRGBO(216, 216, 216, 1),
          child: Image.asset(
            'assets/images/random.jpg',
            fit: BoxFit.fill,
            color: Color.fromRGBO(47, 47, 47, 0.5),
            colorBlendMode: BlendMode.dstATop,
          ),
        ),
        Container(
          height: 85,
          alignment: Alignment.bottomCenter,
          color: Theme.of(context).primaryColor,
          child: BottomTagPanel(),
        ),
      ],
    );
  }
}
