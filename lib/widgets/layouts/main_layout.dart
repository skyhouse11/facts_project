//packages
import 'package:facts_project/widgets/card/card.dart';
import 'package:flutter/material.dart';

//widgets
import '../../widgets/second_layer/custom_app_bar.dart';
import '../second_layer/share_panel.dart';

class MainLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 11,
            child: CustomAppBar(),
          ),
          Expanded(
            flex: 4,
            child: SharePanel(),
          ),
          Expanded(
            flex: 52,
            child: Container(
              margin: EdgeInsets.fromLTRB(16, 10, 16, 101),
              child: CardClass(),
            ),
          ),
        ],
      ),
    );
  }
}
