//packages
import 'package:flutter/material.dart';

//handlers
import '../theme/theme_settings.dart';
import '../theme/theme_colors.dart';

class Themes {
//purpleTheme
  ThemeData orangeTheme = new ThemeData(
    //Text
    textSelectionColor: colorHandler.themes['Orange']
        [settingsHandler.textSelectionColor],
    textSelectionHandleColor: colorHandler.themes['Orange']
        [settingsHandler.textSelectionHandleColor],

    //Buttons
    dialogBackgroundColor: colorHandler.themes['Orange']
        [settingsHandler.dialogBackgroundColor],
    buttonColor: colorHandler.themes['Orange'][settingsHandler.buttonColor],

    //Fons
    primaryColor: colorHandler.themes['Orange'][settingsHandler.primaryColor],
    accentColor: colorHandler.themes['Orange'][settingsHandler.accentColor],
  );
}

Themes themes = new Themes();
