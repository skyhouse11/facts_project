//packages
import 'package:flutter/material.dart';

//handlers
import '../theme/theme_settings.dart';

class ColorHandler {
  Map themes = {
    'Orange': {
      //Text
      settingsHandler.textSelectionColor:
          const Color.fromRGBO(255, 255, 255, 1),
      settingsHandler.textSelectionHandleColor:
          const Color.fromRGBO(245, 202, 128, 1),

      //Main colors
      settingsHandler.primaryColor: const Color.fromRGBO(248, 198, 113, 1),
      settingsHandler.accentColor: const Color.fromRGBO(58, 178, 146, 1),

      //Buttons
      settingsHandler.dialogBackgroundColor:
          const Color.fromRGBO(58, 178, 146, 0.5),
      settingsHandler.buttonColor: const Color.fromRGBO(226, 226, 226, 0.4),
    },
  };
}

ColorHandler colorHandler = new ColorHandler();
