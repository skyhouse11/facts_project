// packages
import 'package:flutter/material.dart';

//layouts
import '../widgets/layouts/background.dart';
import '../widgets/layouts/main_layout.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Container(
          child: Stack(
            children: <Widget>[
              BackgroundLayout(),
              MainLayout(),
            ],
          ),
        ),
      ),
    );
  }
}
